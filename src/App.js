import React, { Component } from 'react';
import './App.css';

// data
import { acuerdos } from './acuerdos.json';

// subcomponents
import MenuAcuerdos from './components/MenuAcuerdos';
import ModalCrearAcuerdo from './components/ModalCrearAcuerdo';
import ModalParticipantes from './components/ModalParticipantes';
import ModalActividades from './components/ModalActividades';

class App extends Component {

  constructor() {
    super();
    this.state = {
      acuerdos
    }
    this.handleAddTodo = this.handleAddTodo.bind(this);
  }

  removeTodo(index) {
    this.setState({
      acuerdos: this.state.acuerdos.filter((e, i) => {
        return i !== index
      })
    });
  }

  handleAddTodo(todo) {
    this.setState({
      acuerdos: [...this.state.acuerdos, todo]
    })
  }

  showParticipantes(todo){

  }

  render() {
    const acuerdos = this.state.acuerdos.map((todo, i) => {
      return (

        <div className="col-md-4" key={i}>
          <div className="card mt-4">

            <div className="card-header">
              <h5>Acuerdo {i+1}
                <button
                  className="btn btn-sm btn-link float-right"
                  onClick={this.removeTodo.bind(this, i)}>
                  Eliminar
                </button>
              </h5>
            </div>

            <div className="card-body">
              <h5>"{todo.nombreAcuerdo}"</h5>
              {todo.descripcion}<br></br>
              <small className="text-muted">{todo.notas}</small><br></br>
              <small className="text-primary">Fecha Límite: {todo.date}</small>
            </div>

            <div className="card-footer">
              <button
                type="button"
                className="btn btn-sm btn-success"
                data-toggle="modal"
                data-target="#ModalParticipantes">
                Participantes
              </button>

              <button
                type="button"
                className="btn btn-sm btn-primary ml-3"
                data-toggle="modal"
                data-target="#ModalActividades">
                Actividades
              </button>
            </div>

          </div>
        </div>
      )
    });

    // RETURN THE COMPONENT
    return (
      <div className="App">

        <MenuAcuerdos contadorAcuerdos="{this.state.acuerdos.length}"></MenuAcuerdos>
        <ModalCrearAcuerdo onAddTodo={this.handleAddTodo}></ModalCrearAcuerdo>
        <ModalParticipantes></ModalParticipantes>
        <ModalActividades></ModalActividades>

        <div className="container-fluid">
          <div className="row">
            {acuerdos}
          </div>
        </div>

      </div>
    );
  }
}

export default App;

