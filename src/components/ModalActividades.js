import React, { Component } from 'react';


class ModalActividades extends Component {

  render() {
    return (

      <div className="modal fade" id="ModalActividades" tabIndex="-1" role="dialog" aria-labelledby="ModalActividadesLabel" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="ModalActividadesLabel">Agregar Actividad</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
                aqui va para agregar actividades
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              <button type="submit" className="btn btn-primary">Agregar</button>
            </div>
          </div>
        </div>
      </div>

    )
  }

}

export default ModalActividades;