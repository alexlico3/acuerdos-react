import React, { Component } from 'react';
import ListItem from './ListItem';
class ListForm extends Component {

  constructor(props) {
      super(props);
      this.items = props.items;
      this.state = {
        name: ''
      };
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
  }

    handleChange(event) {
        this.setState({name: event.target.value});
    }

    handleSubmit(event) {
        this.items.push(this.state);
        this.setState({
            name: ''
        }); 
        event.preventDefault();
    }

  render() {
    return (
        <form className="alert alert-secondary" onSubmit={this.handleSubmit}>
            <div className="form-group">
                <label htmlFor="name">Nombre</label>
                <input className="form-control" id="name" placeholder="Ingresa el nombre" />
            </div>
            <button className="btn btn-success" onClick={this._add_item()}>Agregar</button>
        </form>
    );
  }

  _add_item(){

  }
}

export default ListForm;
