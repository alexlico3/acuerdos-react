import React, { Component } from 'react';
import logo from '../images/logo-acuerdos.png'

class MenuAcuerdos extends Component {
  constructor (props) {
    super(props);

    this.state = {
      contadorAcuerdos: this.props.contadorAcuerdos
    };

  }

  render() {
    return (

      <nav className="navbar navbar-expand-lg navbar-dark">
        <a className="navbar-brand" href="#"> <img src={logo} className="navbar-logo" alt="logo" /> 
          Registro de Acuerdos
          <span className="badge badge-pill badge-light ml-2">
          </span></a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNavDropdown">
          <ul className="navbar-nav">
            <li className="nav-item active">
              <a className="nav-link" data-toggle="modal" data-target="#ModalCrearAcuerdo">Crear Acuerdo <span className="sr-only">(current)</span></a>
            </li>
          </ul>
        </div>
      </nav>
    )
  }

}

export default MenuAcuerdos;