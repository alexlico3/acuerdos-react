import React, { Component } from 'react';

import List from './List';

class ModalParticipantes extends Component {

    constructor(props) {
        super(props);
        this.state = {
          todo: props.todo
        };
    }

    componentDidMount() {

    }

    componentWillUnmount() {
        // clearInterval(this.timerID);
    }

  render() {
    return (

      <div className="modal fade show" id="ModalParticipantes" tabIndex="-1" role="dialog" aria-labelledby="ModalParticipantesLabel" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="ModalParticipantesLabel">Agregar Participante</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
                <List />
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              <button type="submit" className="btn btn-primary">Agregar</button>
            </div>
          </div>
        </div>
      </div>

    )
  }

}

export default ModalParticipantes;