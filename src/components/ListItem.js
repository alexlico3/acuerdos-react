import React, { Component } from 'react';
class ListItem extends Component {

  constructor(props) {
      super(props);
      this.state = {
        item: props.item
      };
  }

  render() {
    return (
        <li className="list-group-item">
            {this.state.item.name}
            <button className="btn btn-danger float-right"><i className="fas fa-trash"></i></button>
        </li>
    );
  }
}

export default ListItem;
