import React, { Component } from 'react';
import ListItem from './ListItem';
class List extends Component {

  constructor(props) {
      super(props);
      this.state = {
        items: [{name: "Prueba"}]
      };
  }

  render() {
      const listItems = this.state.items.map((item, index) =>
          <ListItem key={index} item={item} />
      );
    return (
        <div>
            <form className="alert alert-secondary" onSubmit={this.handleSubmit}>
                <div className="form-group">
                    <label htmlFor="name">Nombre</label>
                    <input className="form-control" id="name" placeholder="Ingresa el nombre" />
                </div>
                <button className="btn btn-success" onClick={this._add_item()}>Agregar</button>
            </form>
            <div className="border">
                <ul className="list-group">{listItems}</ul>
            </div>
        </div>
    );
  }

  _add_item(){

  }
}

export default List;
