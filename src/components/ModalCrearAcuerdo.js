import React, { Component } from 'react';


class ModalCrearAcuerdo extends Component {
  constructor () {
    super();
    this.state = {
      nombreAcuerdo: '',
      descripcion: '',
      date: '',
      notas: '',
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    this.props.onAddTodo(this.state);
    this.setState({
      nombreAcuerdo: '',
      descripcion: '',
      date: '',
      notas: '',
    });
  }

  handleInputChange(e) {
    const {value, name} = e.target;
    console.log(value, name);
    this.setState({
      [name]: value
    });
  }

  onChange = date => this.setState({ date })

  render() {
    return (

      <div className="modal fade" id="ModalCrearAcuerdo" tabIndex="-1" role="dialog" aria-labelledby="ModalCrearAcuerdoLabel" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="ModalCrearAcuerdoLabel">Crear Acuerdo</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form onSubmit={this.handleSubmit} className="card-body">
            <div className="modal-body">
              
                <div className="form-group">
                  <input
                    type="text"
                    name="nombreAcuerdo"
                    className="form-control"
                    value={this.state.nombreAcuerdo}
                    onChange={this.handleInputChange}
                    placeholder="Nombre del Acuerdo"
                    />
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    name="descripcion"
                    className="form-control"
                    value={this.state.descripcion}
                    onChange={this.handleInputChange}
                    placeholder="Descripción"
                    />
                </div>

                <div className="form-group">
                  <input
                    type="text"
                    name="date"
                    className="form-control"
                    value={this.state.date}
                    onChange={this.handleInputChange}
                    placeholder="Fecha Límite"
                    />
                </div>

                <div className="form-group">
                  <input
                    type="text"
                    name="notas"
                    className="form-control"
                    value={this.state.notas}
                    onChange={this.handleInputChange}
                    placeholder="Notas"
                    />
                </div>

              
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              <button type="submit" className="btn btn-primary">Agregar</button>
            </div>
</form>
          </div>
        </div>
      </div>
    )
  }

}

export default ModalCrearAcuerdo;